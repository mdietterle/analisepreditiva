+++
title = "Prof Martim"
[data]
baseChartOn = 3
colors = ["#627c62", "#11819b", "#ef7f1a", "#4e1154"]
columnTitles = ["Section", "Status", "Author"]
fileLink = "content/projects.csv"
title = "Projects"

+++
{{< block "grid-2" >}}
{{< column >}}

# Bem vindo ao site do **Prof. Martim**.

Neste site você vai encontrar o conteúdo das matérias de Sistemas Operacionais, Banco de Dados, Análise Preditiva e Ciência de Dados.

{{< tip "warning" >}}
Use os links no menu superior para navegar no site. {{< /tip >}}

{{< tip >}}
O material da aula vai estar no [blog](./blog).

Or, [generate graphs, charts](docs/compose/graphs-charts-tables/#show-a-pie-doughnut--bar-chart-at-once) and tables from a csv, ~~or a json~~ file.
{{< /tip >}}

{{< button "docs/compose/" "Read the Docs" >}}{{< button "https://github.com/onweru/compose" "Download Theme" >}}
{{< /column >}}

{{< column >}}
![diy](/images/scribble.jpg)
{{< /column >}}
{{< /block >}}
