---
title: "[Banco de Dados] - Aula 1 - Introdução"
date: 2023-02-10T15:10:22-03:00
image: capa_database1.jpg
draft: false
---
Em 1597, [Sir Francis Bacon](https://pt.wikipedia.org/wiki/Francis_Bacon) publicou a primeira aparição desta conhecida e amplamente utilizada frase: “Conhecimento é poder”. Certamente, ele não estava falando sobre sistemas de informação como os conhecemos na indústria de tecnologia, mas a frase ainda tem um enorme significado, pois reconhece o valioso potencial e capacidade que vem com informações perspicazes que se tornam conhecimento.

Mas de onde vêm as informações? O ecossistema de tecnologia é orientado por dados e encontrar valor nos dados está se tornando crítico para negócios bem-sucedidos, que é o tópico deste artigo: dados e informações. Cobrimos dados versus informações para entender melhor sua interdependência, seus pontos de diferença e como um não pode existir sem o outro. Vamos começar definindo cada conceito.

## O que são Dados

Dados são “material bruto, não analisado, desorganizado, não relacionado e ininterrupto que é usado para obter informações após a análise”. Essencialmente, os dados são fatos simples, observações, estatísticas, caracteres, símbolos, imagens, números e muito mais que são coletados e podem ser usados para análise. Os dados deixados sozinhos não são muito informativos e, nesse sentido, são relativamente sem sentido, mas ganham propósito e direção depois que são interpretados para obter significado.

Quer sejam qualitativos ou quantitativos, os dados são um conjunto de variáveis que ajudam a construir resultados. Outra característica fundamental dos dados é que eles são autônomos e não dependem de nenhum outro conceito para existir, ao contrário da informação que só existe por causa dos dados e é totalmente dependente deles.

{{< block "imagem1" >}}
{{< picture "dados.png" "dados.png" "Dados" >}}
{{< /block >}}

Dados e informações são medidos em bits e bytes. Pode ser representado em tabelas estruturadas/não estruturadas, gráficos, árvores, etc., e não tem significado até que seja analisado para atender às necessidades específicas de um usuário.

## O que é informação?

Informação é o conjunto de dados que já foi processado, analisado e estruturado de forma significativa para se tornar útil. Uma vez que os dados são processados e ganham relevância, eles se tornam informações totalmente confiáveis, seguras e úteis.

De acordo com este artigo da Forbes, a informação é “dados preparados que foram processados, agregados e organizados em um formato mais amigável que fornece mais contexto. As informações geralmente são fornecidas na forma de visualizações de dados, relatórios e painéis.”

A informação atende aos requisitos de um usuário, dando-lhe significado e utilidade, pois é o produto de dados que foram interpretados para fornecer um significado lógico. Como dissemos, a informação não pode existir sem seu bloco de construção: os dados. Uma vez que os dados são transformados em informações, eles não contêm detalhes inúteis, pois todo o seu propósito é possuir contexto, relevância e propósito específicos.

{{< picture "info.jpeg" "info.jpeg" "Informações" >}}

Em última análise, o objetivo de processar dados e transformá-los em informações é ajudar as organizações a tomar decisões melhores e mais informadas que levam a resultados bem-sucedidos.

Para coletar e processar dados, as organizações utilizam Sistemas de Informação (SI) que são uma combinação de tecnologias, procedimentos e ferramentas que reúnem e distribuem as informações necessárias para a tomada de decisões.

## O que é conhecimento?

Conhecimento significa a familiaridade e consciência de uma pessoa, lugar, eventos, ideias, questões, maneiras de fazer as coisas ou qualquer outra coisa, que é obtida por meio do aprendizado, percepção ou descoberta. É o estado de conhecer algo com cognição através da compreensão de conceitos, estudo e experiência.
{{< block "grid-3" >}}
{{< column >}}
as
{{< /column >}}
{{< column >}}
{{< picture "knowledge.jpg" "knowledge.jpg" "Conhecimento" >}}
{{< /column >}}
{{< column >}}
as
{{< /column >}}
{{< /block >}}

Em poucas palavras, o conhecimento conota a compreensão teórica ou prática confiante de uma entidade, juntamente com a capacidade de usá-la para um propósito específico. A combinação de informação, experiência e intuição leva ao conhecimento que tem o potencial de tirar inferências e desenvolver insights, com base em nossa experiência e, assim, pode auxiliar na tomada de decisões e ações.

## De forma prática...

Uma forma prática de entendermos e diferenciarmos é fazer a seguinte analogia:

**Dado:** 5 -> Um número

**Informação:** 5A -> Agora, sabemos que p número tem relação com energia elétrica (corrente)

**Conhecimento:** 5A pode matar!

Outro exemplo:

**Dados:** 4a 61 6e 65 20 44 6f 65 2c 0a 34 20 53 74 72 65 65 74 2c 0a 44 61 6c 6c 61 73 2c 20 54 58 20 39 38 31 37 34 0a

**Informação:** 

João da Silva; 

Rua Imigrantes, 500; 

Jaraguá do Sul

**Conhecimento:** É o endereço da faculdade

## Porque essas definições são importantes?

Estamos vivendo em uma era de conhecimento intensivo. As empresas estão investindo pesadamente na condução de seus dados e informações em “ insights acionáveis ”, outra palavra da moda popular que ocorre quando o conhecimento adquirido leva a ações e melhorias significativas de sua empresa e de sua equipe.

Mas mal-entendidos sobre a natureza dos dados, informações e conhecimento, e como eles estão interligados, estão levando as empresas a ficarem aquém de seus objetivos. Muitas empresas equivocadamente afogam seus sistemas com cada vez mais dados e informações em uma abordagem de “jogar tudo na parede e ver o que cola”, sem considerar que a geração de conhecimento, na verdade, requer um processo ativo e habilidoso.

Essas questões decorrem do equívoco de que informação equivale a conhecimento, quando esse não é o caso. Só porque um funcionário pode ter um monte de informações sobre um determinado assunto, isso não significa que ele tenha experiência e intuição para chegar a conclusões, julgamentos de valor ou decisões informadas. Isso, nossos amigos, é conhecimento.

E, mesmo quando o conhecimento valioso flui em abundância por toda a organização, as empresas precisam de uma estrutura de gerenciamento de conhecimento bem-sucedida para capturá-lo e otimizá-lo. Sem isso, o conhecimento corre o risco de não chegar às pessoas certas na hora certa ou não ser embalado de forma que os funcionários entendam sua utilidade e valor para eles, perdendo-se na tradução. É por isso que a gestão do conhecimento nas organizações é tão crucial.

Afinal, conhecimento sem gestão é como uma meta ambiciosa sem planejamento: uma oportunidade perdida.

Como Sir Francis Bacon já dizia, **CONHECIMENTO É PODER!!**
